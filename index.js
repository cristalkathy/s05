//Methematical Operations (-, *, /, %)
//Subtraction
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3);//-0.5 results in proper mathematical operation
console.log(num3-num4);//5 results in proper mathematical operation
console.log(numString1-num2);//-1 results in proper mathematical operation because the string was forced to become number
console.log(numString2-num2);
console.log(numString1-numString2);//-1, In subtraction, numeric strings will not concatinate
//and instead will be forcibly changed its type and subtract properly

let sample2 ="Juan Dela Cruz";
console.log(sample2-numString1);//NaN - not a number. When trying to perform subtration between alphanumeric string asnd numeric string,  the result is NaN.

//Multiplication
console.log(num1*num2);
console.log(numString1*num1);
console.log(numString1*numString2);
console.log(sample2*5);

let product1 = num1*num2;
let product2 = numString1*num1;
let product3 = numString1*numString2;
console.log(product1);

//Division
console.log(product1/num2);
console.log(product2/5);
console.log(numString2/numString1);
console.log(numString2%numString1);

//Division/multiplication by 0
console.log(product2*0);
console.log(product3/0);
//Division by 0 is not accurately 

//% Module - remainder of division operation
console.log(product2%num2);
console.log(product3%product2);
console.log(num1%num2);
console.log(num1%num1);

//true and false boolean
let isMaried = false;
let isMVP = true;
let isAdmin = true;
//you can also concatenate strings + boolean
console.log("is she married? "+isMaried)
console.log("is he mvp? "+isMVP)
console.log("is he the current admin? "+isAdmin);

//Arrays
/*
	Arrays 
		-special kind of data type to store multple values
		- can actually store data with different types BUT as the best practice,
		arrays are used to contain multiple values of the SAME data type
		-values in an arrays are separated by commas
	An array is created with an array literak = []

*/

//Syntax:
	//Let arrayName=[elementA,elementB,elementC,...]

let array1 = ["Goku","Picolo","Gohan","Vegeta"]
console.log(array1)
let array2 =["One Punch Man",true,500,"Saitama"]
console.log(array2)

//Array are better though of as group data

let grade = [98.7,92.1,90.2,94.6]
console.log(grade)

//Objects

//MiniActivity
let svt = ['Choi Seungcheol','Yoon Jeonghan','Hong Jisoo','Wen Junhui',
'Kwon Soonyoung','Jeon Wonwoo','Lee Jihoon','Seo Myungho','Kim Miingyu',
'Lee Seokmin','Chwe Hansol','Boo Seungkwan','Lee Chan']
console.log(svt)

let person = {
	firstname:'Katherine',
	lastname:'Cristal',
	isDeveloper:false,
	hasPortfolio:false,
	age: 20,
	contact: "+639167453238",
	address: {
		 houseNumber:'345',
		 street:'Diamond',
		 city:'Manila'
	}
}
console.log(person);

//Undefine vs Null
	//Null - is explicit absence of data/value. This is done to project that a variable contains over undefined.
let sampleNull
let sampleUndefined;
console.log(sampleNull)
console.log(sampleUndefined);

//certain processes in programming explicitly return null to indicate that the task resulted to nothing

let foundResult = null;

//for undefined, this is normally caused by developers creating variables that have no value or data/associated with them
//This is when a variable does exist but its value is stiill unknown

let person2={
	name:"Peter",
	age: 35
}
console.log(person.isAdmin)
/* [SECTION] Functions
	Functions
		- in JavaScript are line/block of codesthat tell our device/application to perform a certain task when called/invoked
		- are reusable pieces of code with instructions which used OAOA just as long as we can call/invoke them

		Syntax:
			function functionName(){
				code block
					- block of code that will be executed oncethe  function has been run/called/invoked	
			}

*/

// Declaring function
function printName1(){
	console.log("Aurelius of Eskia");
};

//invoking/calling function - functionName()
printName();
printName();
printName();

function showSum(){
	console.log(19+20);
};

showSum();

// Note: do not create functions with the same name

/* Parameters and Arguements
	"name" is called parameter
		a parameter acts as a named variable/container that exists ONLY inside the function. This isused to store information/ to act as a stand-in or simply the contain the value passed into  thefunction as an arguement.
*/

function printName(prince){
	console.log(`${prince}, Youngest Prince of Eskia`);
};

// When a function is invoked and a data is passed, we call the data as arguement
// In this invocation, "Aurelius" is an arguement passed into our printName function and is represented by the "prince" parameter within our function.
// data passed into the function: arguement
// representation of the arguement within the function: parameter
printName("Aurelius");

function displayNum(number){
	alert(number);
};

displayNum(9000);

//Mini Activity
function displayMessage(m){
	console.log(`${m} is fun!`);
};
displayMessage("JavaScript");
displayMessage("C++");
displayMessage("Java");

// Multiple parameters and arguements
function displayFullName(firstName, mi, lastName, age){
	console.log(`${firstName} ${mi} ${lastName} ${age}`);
}

displayFullName("Vil", "E", "Schoenheit", 18);

// return keyword
	// The "return" statement allows the output of a function to be passed to the the line/block of code that invoked/called the function
	// any line/block that comes after the return statement is ignored because it ends the function execution

	function createFullName(firstName, middleName, lastName){
		// return is used so that a function may return a value
		// it also stops the process of the function any other instruction after the keyword will not be  processed
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned.")
	};

	let fullName1 = createFullName("Min", "SUGA", "Yoongi");
	let fullName2 = displayFullName("Kim", "RM", "Namjoon");
	let fullName3 = createFullName("Jung", "J-HOPE", "Hoseok")

	console.log(fullName1);
	console.log(fullName2);
	console.log(fullName3);